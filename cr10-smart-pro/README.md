# CR-10 Smart Pro Klipper Config

## Machine Specs

- Board: `CR-FDM-v2.5.S1`
- Klipper Controller: `BTT Pad 7`

## Firmware Configuration
To use this config, during "make menuconfig" select the STM32F103
with a "64KiB bootloader" and serial (on USART1 PA10/PA9)
communication. Enable PA0 GPIO pin on startup.
