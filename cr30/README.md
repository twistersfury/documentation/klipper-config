# CR-30 Klipper Configuration

## Machine Specs

- Board: `BigTreeTech SKR Mini EZ v3`
- Motors: `NAK3D LDO Stepper Motor and Mount Upgrade Kit For Creality CR-30`
- Rails: `Linear Rails For X & Y`

## Motor Specs

- 2.0 Max Current
- 160 Steps Per MM

## Board Notes:

The SKR Mini EZ v3 does not have all the same ports as the stock board. Specifically, the following things should be noted:

1. `FAN0` - Part Cooling Fan
2. `FAN1` - Heat Cartridge Fan
3. `FAN2` - MCU/Control Board Fan
4. `E0 Stop` - Filament Sensor
5. `Z-Probe` - The CR-30 doesn't have a Z-Stop or a Z-Probe. It does however use a 3-Pin Optical Sensor, which the Mini E3 does not have. In order to get the optical sensor working, swap out the 3-PIN connector with a 5-PIN. When the 3-Pin is plugged into the stock board, starting at the pin closest to the center of the board, you have power, ground, and signal. With a 5-PIN connector, plugged into the board, starting at the closet to center pin, it is signal, blank, blank, power, ground. If you don't have a 5-PIN, you can also use dupont connectors.  

## Installation Instructions

Follow Instructions Per [Official Instructions](https://www.klipper3d.org/Installation.html). See Menu Configuration For `make menuconfig`. To flash, copy `out/klipper.bin` to local sd card and rename to `firmware.bin`. Insert SD Card Into printer and power cycle the printer.

## Menu Configuration

- Enable extra low-level configuration: Selected/On
- Micro-controller Architecture: `STMicroelectronics STM32`
- Processor Model: `STM32G0B1`
- Bootloader offset: `8KiB bootloader`
- Clock Reference: `8 MHz crystal`
- Communication Interface: `USB (on PA11/PA12)`