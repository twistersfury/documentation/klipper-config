# Klipper Configuration

This repo contains the Klipper Configuration multiple printers. It is set up in a way that allows shared configurations. 
Each printer has its own configuration folder that includes the shared configuration automatically.

Due to the way klipper works, including the same `gcode_macro` more than once will overwrite the previous version. In this way,
printers can override specific configurations as needed.

# Installation

The simplest installation method is to use rsync:

```shell
rsync -av ./ user@server:/path/to/klipper/config/tf_klipper --exclude=.git --exclude=.idea --delete
```

This will create a folder in the config folder called `tf_klipper`. Once done, point the raw printer.cfg to the correct version:

```shell
ssh user@server 'echo "[include tf_klipper/cr30/printer.cfg]" > /path/to/klipper/config/printer.cfg'
```

# Gotchas

## SAVE_CONFIG

Due to the inclusion nature of this setup, you will see two distinct 'changes' in the way SAVE_CONFIG works.

1. All SAVE_CONFIG changes will be written directly to `printer.cfg`. While this is normal, this does mean that no changes will be written to your actual configuration. This is useful in that it means that items like BED_MESH configurations will be retained post update.
2. Some changes to SAVE_CONFIG, like `bltouch` offsets will fail to save (resulting in an error message on save) as they are in sub files and SAVE_CONFIG only touches printer.cfg. I just manually update the offset.